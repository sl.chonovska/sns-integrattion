from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

# Make sure everything is required!

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = models.DateField(null=True, blank=True)
    avatar = models.ImageField(upload_to='avatars', blank=True)
    phone_number = PhoneNumberField(null=True, blank=True, unique=False)
    facebook_id = models.CharField(max_length=15,null=True, blank=True) # idk what this is exactly
    twitter_id = models.CharField(max_length=20, null=True, blank=True) # idk what this is exactly
    instagram_id = models.CharField(max_length=20, null=True, blank=True) # idk what this is exactly


class FaceboookProfiles(models.Model):
    fb_id = models.CharField(max_length=15)
    fb_user_token = models.CharField(max_length=220)

    def __str__(self) -> str:
        return self.fb_id

class RelativeProfile(models.Model):
    RELATIONSHIPS = (
        ('D','Daughter'),
        ('S','Son'),
        ('B', "Brother"),
        ('S', "Sister"),
        ('NI','Niece'),
        ('NE','Nephew'),
        ('GD','Granddaughter'),
        ('GS','Grandson'),
        ('C','Cousin'),
        ('F','Father'),
        ('M','Mother'),
        ('GF','Grandfather'),
        ('GM','Grandmother'),
        ('U','Uncle'),
        ('A','Aunt'),
        ('F','Friend'),
    )


    relative = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    first_name =  models.CharField(max_length=50)
    last_name =  models.CharField(null=True,max_length=50,blank=True)
    birthday = models.DateField(null=True, blank=True,)
    avatar = models.ImageField(upload_to='avatars', blank=True, default='default-avatar.png')
    avatar_url = models.URLField(max_length=1000,blank=True,null=True)
    relation = models.CharField(max_length=2, choices=RELATIONSHIPS)
    facebook_id = models.ForeignKey(FaceboookProfiles, on_delete=models.SET_NULL, null=True,blank=True)
    twitter_id = models.CharField(max_length=20,null=True, blank=True) 
    instagram_id = models.CharField(max_length=20,null=True, blank=True) 


class SavedPosts(models.Model):

    SNS = (
        ('F','facebook'),
        ('I','instagram'),
        ('T','twitter'),
    )

    post_id =  models.CharField(max_length=31)
    sns_tag = models.CharField(max_length=1, choices=SNS)
    saved_date = models.DateField()
    relative = models.ForeignKey(RelativeProfile, on_delete=models.CASCADE)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE)

class DeletedPosts(models.Model):

    SNS = (
        ('F','facebook'),
        ('I','instagram'),
        ('T','twitter'),
    )

    post_id =  models.CharField(max_length=31)
    sns_tag = models.CharField(max_length=1, choices=SNS)
    saved_date = models.DateField()
    relative = models.ForeignKey(RelativeProfile, on_delete=models.CASCADE)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE)

def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])
        # print(kwargs['sender'])

#delete?
#def create_relative(sender, **kwargs):
    #if kwargs['created']:
        #creation = RelativeProfile.objects.create(relative=kwargs['instance'])
    #print(kwargs)
post_save.connect(create_profile, sender=User)
#post_save.connect(create_relative, sender=UserProfile)




