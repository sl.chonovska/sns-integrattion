from django.core.validators import URLValidator
from django import template
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
import base64
import requests
register = template.Library()

#custom filter


def checkLink(value):
    validate = URLValidator()
    try:
        validate(value)
        text =f'<a href={value} target=”_blank”>{value}</a>'
        return mark_safe(text)
    except ValidationError as exception:
        return value

register.filter("checkLink",checkLink)

def base64Images(link):
    img_url_og = str(link)
    img_url = base64.b64encode(requests.get(img_url_og).content).decode('utf-8')
    return img_url

register.filter("base64Images",base64Images)

def base64Videos(link):
    video_url_og = str(link)
    video_url = base64.b64encode(requests.get(video_url_og).content).decode('utf-8')
    return video_url

register.filter("base64Videos",base64Videos)
