from django import forms
from django.db import models
from django.db.models import fields
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import RelativeProfile, UserProfile
from phonenumber_field.modelfields import PhoneNumberField
from django.core.exceptions import ValidationError
from config.settings import *

class ImageWidget(forms.widgets.ClearableFileInput):
    template_name = "widgets/image_widget.html"


class UserRegForm(UserCreationForm):
    class Meta:
        model = User
        fields =  ("username","password1","password2")
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder':"grapevine123"})


class UserDetailForm1(forms.ModelForm):
    class Meta:
        model = User
        fields =  ("first_name","last_name")
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'class':"form-control",'placeholder':"First name",'aria-label':"First name"})
        self.fields['last_name'].widget.attrs.update({'class':"form-control",'placeholder':"Last name",'aria-label':"Last name"})


class UserDetailForm2(forms.ModelForm):
    phone_number = PhoneNumberField()
    birthday = forms.DateField(required=False,widget=forms.DateInput(attrs={'type': 'date'}))
    class Meta:
        model = UserProfile
        fields =  ("phone_number","birthday")
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone_number'].widget.attrs.update({'onfocusout':"process(event)"})

class RelativeForm(forms.ModelForm):
    birthday = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'})) 
    class Meta:
        model = RelativeProfile
        fields = ("first_name","last_name","relation","birthday")

class RelativePhoto(forms.ModelForm):
    avatar_url = forms.URLField(required=False,widget=forms.DateInput(attrs={'type': 'hidden'})) 
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['avatar'].widget.attrs.update({'onchange' : "upload_img(this)"})
        #self.fields['avatar_url'].widget.attrs.update({'type' : "hidden"})
    class Meta:
        model=RelativeProfile
        fields=("avatar","avatar_url",)
        widgets = {"avatar": ImageWidget} 

    class Media:
        js = ('js/preview.js',)
        

class RelativeSNSForm(forms.ModelForm):
    class Meta:
        model = RelativeProfile
        fields = ("facebook_id","twitter_id","instagram_id")

    def clean(self):
        print("Changed data: ",self.changed_data)
        current_user = self.instance.relative
        relatives = current_user.relativeprofile_set.all()
        #twitter
        import twitter
        twitter_handle = self.cleaned_data["twitter_id"]
        api = twitter.Api(consumer_key=API_Key,
                      consumer_secret=API_Key_Secret,
                      access_token_key=Access_Token,
                      access_token_secret=Access_Token_Secret, 
                      )
        if twitter_handle and 'twitter_id' in self.changed_data:
            try:
                twitter_user = api.GetUser(screen_name = twitter_handle)         
            except twitter.TwitterError:
                raise ValidationError(
                    "The Twitter handle name is incorrect!"
                )
            twitter_ids = twitter_user.id
            print("twitter id:" ,type(twitter_ids))
            for relative in relatives:
                rel_twitter = relative.twitter_id
                if rel_twitter and int(rel_twitter)==int(twitter_ids) :
                    raise ValidationError(
                    "The Twitter profile is already added!"
                    )

        #instagram
        instagram_handle = self.cleaned_data["instagram_id"]
        if instagram_handle and 'instagram_id' in self.changed_data:
            import instagrapi
            from instagrapi import Client

            cl = Client()
            if os.path.exists(IG_CREDENTIAL_PATH):
                cl.load_settings(IG_CREDENTIAL_PATH)
                cl.login(IG_USERNAME, IG_PASSWORD)
            else:
                cl.login(IG_USERNAME, IG_PASSWORD)
                cl.dump_settings(IG_CREDENTIAL_PATH)
            try:
                instagram_id = cl.user_id_from_username(instagram_handle)
            except instagrapi.exceptions.UserNotFound:
                raise ValidationError(
                    "The Instagram handle name is incorrect!"
                )
            for relative in relatives:
                rel_instagram = relative.instagram_id
                if rel_instagram and int(rel_instagram)==int(instagram_id) :
                    raise ValidationError(
                    "The Instagram profile is already added!"
                    )
        #facebook
        facebook_profile = self.cleaned_data["facebook_id"]
        if facebook_profile and 'facebook_id' in self.changed_data:
            fb_id = facebook_profile.fb_id
            for relative in relatives:
                rel_facebook = relative.facebook_id
                if rel_facebook and int(rel_facebook.fb_id)==int(fb_id) :
                    raise ValidationError(
                    "The Facebook profile is already added!"
                    )


                

                







    



