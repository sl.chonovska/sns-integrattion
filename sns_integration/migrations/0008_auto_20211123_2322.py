# Generated by Django 2.2.5 on 2021-11-23 14:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sns_integration', '0007_auto_20211117_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relativeprofile',
            name='twitter_id',
            field=models.CharField(max_length=19),
        ),
    ]
