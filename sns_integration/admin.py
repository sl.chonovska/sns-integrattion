from django.contrib import admin
from sns_integration.models import UserProfile,RelativeProfile, FaceboookProfiles, SavedPosts, DeletedPosts
# # Register your models here.

admin.site.register(UserProfile)
admin.site.register(RelativeProfile)
admin.site.register(FaceboookProfiles)
admin.site.register(SavedPosts)
admin.site.register(DeletedPosts)