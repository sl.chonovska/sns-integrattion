function upload_img(input) {
    document.getElementById('help').innerText = ""
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        document.getElementById('img_id').src=e.target.result;
      }
      reader.readAsDataURL(input.files[0]);
    }
    document.getElementById("id_avatar_url").value = "";
}

function upload_img_url(base64_url,url){
    if( url === ''){
        document.getElementById('help').innerText = "You haven't added this SNS!"
    }
    else{
        document.getElementById('help').innerText = ""
        document.getElementById("id_avatar_url").value = url;
        document.getElementById('img_id').src= "data:image/jpeg;base64," + base64_url; 
    }
}
