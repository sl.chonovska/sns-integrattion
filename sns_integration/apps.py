from django.apps import AppConfig


class SnsIntegrationConfig(AppConfig):
    name = 'sns_integration'


