from django.urls import path
from django.urls.conf import re_path
from . import views as v
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('', v.SearchedRedirectView.as_view()),
    path('index/', v.index, name="index"),
    path('register/intro', v.register_intro, name="register_intro"),
    path('register/info/step/1', v.register_info1, name="register_info1"),
    path('register/info/step/2', v.register_info2, name="register_info2"),
    path('register/info/step/3', v.register_info3, name="register_info3"),
    path('register/relative', v.relative_info, name="relative_info"),
    path('register/relative/<int:pk>/photo', v.relative_photo, name="relative_photo"),
    path('register/relative/<int:pk>/sns', v.relative_sns, name="relative_sns"),
    path('home/', v.home, name="home"),
    path('home/filter/', v.filter, name="filter"),
    re_path(r'^home/filter/=(?P<relatives>(?:\d+,*)+)/$',v.filter_home, name='filter_home'),
    path('famtree/', v.famtree, name="famtree"),
    path('famtree/sortby=<str:relation>', v.famtree_sort, name="famtree_sort"),
    path('relative/<int:pk>/', v.RelativeDetailView.as_view(), name="relative-detail"),
    path('relative/<int:pk>/edit', v.RelativeUpdateView.as_view(), name="relative-update"),
    path('relative/<int:pk>/delete', v.RelativeDeleteView.as_view(), name="relative-delete"),
    path('relative/<int:pk>/edit/photo', v.relative_photo, name="relative-update-photo"),
    path('relative/<int:pk>/edit/sns', v.relative_sns, name="relative-update-sns"),
    path('photobook/', v.photobook, name="photobook"),
    path('photobook/filter/', v.filter, name="filter_p"),
    re_path(r'^photobook/filter/=(?P<relatives>(?:\d+,*)+)/$',v.filter_photobook, name='filter_photobook'),
    path('help/', v.help, name="help"),
    path('help_dashboard/', v.help_dashboard, name="help_dashboard"),
    path('help_famtree/', v.help_famtree, name="help_famtree"),
    path('help_addpeople/', v.help_addpeople, name="help_addpeople"),
    path('help_album/', v.help_album, name="help_album"),
    path('settings/', v.settings, name="settings"),
    path('settings_profile/', v.settings_profile, name="settings_profile"),
    path('settings_profile/edit', v.settings_profile_edit, name="settings_profile_edit"), # settings edit profile
    path('settings_notification/', v.settings_notification, name="settings_notification"),
    path('settings_privacy/', v.settings_privacy, name="settings_privacy"),
    path('settings_screen/', v.settings_screen, name="settings_screen"),
    path('settings_language/', v.settings_language, name="settings_language"),
    path('settings_app_information/', v.settings_app_information, name="settings_app_information"),
    path('save/post/<str:post_id>/from/<str:sns_tag>/relative/<int:relative_pk>', v.save_post, name="save_post"),
    path('remove/post/<str:post_id>/from/<str:sns_tag>/relative/<int:relative_pk>', v.remove_post, name="remove_post"),
    path('remove/saved_post/<str:post_id>/relative/<int:relative_pk>', v.remove_saved_post, name="remove_saved_post"),
    path('accounts/login/', v.LoginUserView.as_view(), name="login"),
    path('accounts/logout/', LogoutView.as_view(template_name= 'logout.html',next_page= 'login'), name="logout"),

]