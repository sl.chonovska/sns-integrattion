from ast import Continue
import re
from django.contrib.messages.api import success
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, HttpResponse
import requests
from django.urls import reverse
from django.contrib.auth.views import LoginView
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, UpdateView
from functools import wraps
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import resolve_url
# from django.utils.decorators import available_attrs
from urllib.parse import urlparse

import phonenumbers
from sns_integration.models import DeletedPosts, FaceboookProfiles, SavedPosts
from .forms import *
from config.settings import *
from datetime import datetime

from instagrapi import Client
import twitter
from pyfacebook import GraphAPI


import os
import os.path
import base64
import time

# Create your views here.

#TikTok

#instagram
cl = Client()
#twitter
api = twitter.Api(consumer_key=API_Key,
                      consumer_secret=API_Key_Secret,
                      access_token_key=Access_Token,
                      access_token_secret=Access_Token_Secret,
                      tweet_mode='extended' 
                      )

#convert media to base64
def convertBase64(link):
    if link:
        media_url_og = str(link)
        media_url = base64.b64encode(requests.get(media_url_og).content).decode('utf-8')
        return media_url
    return link

#load facebook test people
def load_test_users():
    user_id = ['263364355631075','108486498291782','101563002392208','105945798609647','108470741689155','108902648311519']
    access_tokens = ['EAACdZC9xZClo4BAJOrQ89Bcz7tk6xGCIAZBOX9kpnamiCy41GXZCBsHy5YNPYG3xg0YXxW2F0cFqGtWKjlaXZAZBTTyzSIIG9rVWKThivUPXIhWZCxIUYcwS2CdHOZBhRhlB396G2wmYZBA6Yyaibrl4nJf1DA0oeVveondR6enGm7iVyCmIZCxQ3f',
                    'EAACdZC9xZClo4BAOX68ZASPyxsK5mghBUAKficQVFTk0IJVSqE5AF22bB6OGsGrAfFj3Mxgsu7et7846exyVk38xKe7ql5ZC2VtCRcQLhIk01rNOpL1xaEmIQnZArKtjBxOPvakjvHEQ0QdZA0081ZB8vkYRpFvwDZAw3hesZABB6AY3ZCCwCa2JzZC',
                    'EAACdZC9xZClo4BAA0ijKFZAd6G4b5MHtnTPZCbvmseDymVeDZA5zsQoBYZCaEFiXeiPE1GCUGmtp7uPptREKDZCNKuPXO6zP8RvSsGYq11trxYIlfo3479gxGg7qxHgiZAxz9QvZBSiiZBGkfksXZAYZBfYJx5bQzjI9g6anTldFkkU4MOKJ035rI1Mg',
                    'EAACdZC9xZClo4BACr7ElZCr7Qyb4I0pUppvwNxnFxN2S63z8ETsuZBpZA17IyjUlexZBSJZBZC5NoOmQPR7qdcuMeHlPcLvUnZChDCwfrLGgwTF6r3ZCVGeAN8EBoheLDFZC3Ry7YTT0rdb0be9AZC9iI9s3qItur6fmNG80s3M8apMZBxut34WAwzKnC',
                    'EAACdZC9xZClo4BAP9HahG9frxxZAtdPGUhvZCrpmpN9YyMPKZABDUyhkGOLzeaNAHL8TJBO7uCxljiKbWZAYgLFowyD6ZBCK1wMUy1zAV66N9ULHoDfPkLwAu4nWG3Tizo9q3pLKnaYsLZCzw6f7MjEic4BTQAswVndyYoaT3T9rV3oy4ect9ElK',
                    'EAACdZC9xZClo4BACYaZBZCa1bngkDmFx2jOF1784koS789ssEnaYLesRAhCKTiMKRipij1dFaIEUuBZAZB4Ub0Q34EgxFFfcsIZAWKnhy7iHuZA6W6uTslRLcELKeugKOk8K4CpttMuwdixmCr1bPJ0ffHh3HtvHW6k3eTeucFRrJKujaay7JwBh']
    for i in range(6):
        FaceboookProfiles.objects.create(fb_id = user_id[i], fb_user_token = access_tokens[i])

class LoginUserView(LoginView):
    template_name= 'sns-integration/login.html'
    def get_success_url(self):
        url = self.get_redirect_url()
        if url:
            return url
        else:
            return reverse("home")

def my_user_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # the following line is the only change with respect to
            # user_passes_test:
            if request.user.is_authenticated:
                if test_func(request.user, *args, **kwargs):
                    return view_func(request, *args, **kwargs)
                raise PermissionDenied
            path = request.build_absolute_uri()
            resolved_login_url = resolve_url(login_url or LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(
                path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator


def user_check(user, **kwargs):
    rel = RelativeProfile.objects.filter(relative = UserProfile.objects.get(user=user))
    for r in rel:
        if kwargs['pk'] == r.pk:
            return True
    return False


class SearchedRedirectView(RedirectView):
    url="index/"

def index(req):
    # print(req.user.email)
    if req.user.is_authenticated:
        return redirect("home")
    else: 
        return render(req, "sns-integration/index.html")
@login_required
def home(req):
    handles =[]
    insta_handles = []
    fb_handles = []
    user = UserProfile.objects.get(user=req.user)
    relatives = user.relativeprofile_set.all()
    for info in relatives:
        if info.twitter_id:
            handles.append(info.twitter_id) 
        if info.instagram_id:
            insta_handles.append(info.instagram_id)
        if info.facebook_id:
            fb_handles.append(info.facebook_id) 
    
            
    print("okay")
    print(handles,insta_handles,fb_handles)
    contents = {"user": req.user,
                "profile": UserProfile.objects.get(user=req.user),
                #"tweets": getTweets(handles),
                #"insta_posts": get_instaPosts(insta_handles),
                'posts': getSnsPosts(handles, insta_handles,fb_handles,user)
                }

    return render(req, 'sns-integration/home.html',contents)
from django.db.models import Count
@login_required
def famtree(req):
    sort = False
    if req.method == "POST":
        if req.POST.getlist("post_categories"):
        #filter = ','.join(map(str, req.POST.getlist("post_categories")))
            return redirect('famtree_sort',relation=str(req.POST.getlist("post_categories")[0]))
    result = []
    users = UserProfile.objects.get(user=req.user)
    relatives = users.relativeprofile_set.all()
    relationships = relatives.values_list('relation', flat=True).distinct()
    for r in relationships:
        if r == '':
            result.append({'short':None,'relation':None})
            continue
        choice = {k: v for k, v in RelativeProfile.RELATIONSHIPS}[r]
        result.append({'short':r,'relation':choice})
    result1 = (relatives
    .values('relation','first_name','last_name','pk','avatar','avatar_url')
    .annotate(dcount=Count('relation'))
    .order_by('relation'))
    for info in result1:
        if info['avatar']:
            info['avatar'] = RelativeProfile.objects.get(pk = int(info['pk']))
    count = relatives.count()
    content = {
        'relatives':result1,
        'count':count,
        'relationships': relationships,
        'relation_display': result,
        'sort':sort
        
    }
    return render(req, 'sns-integration/famtree.html',content)

@login_required
def famtree_sort(req,relation):
    sort = True
    result = []
    users = UserProfile.objects.get(user=req.user)
    relatives = users.relativeprofile_set.all()
    #RelativeProfile.objects.filter(pk__in=relatives)
    relatives_sort = relatives.filter(relation = relation)
    relationships = relatives.values_list('relation', flat=True).distinct()
    if relation not in relationships:
        return redirect('famtree')
    for r in relationships:
        if r == '':
            result.append({'short':None,'relation':None})
            continue
        choice = {k: v for k, v in RelativeProfile.RELATIONSHIPS}[r]
        result.append({'short':r,'relation':choice})
    count = relatives_sort.count()
    content = {
        'relatives':relatives_sort,
        'count':count,
        'relation': relatives_sort[0].get_relation_display(),
        'relationships': relationships,
        'relation_display': result,
        'sort':sort
    }
    return render(req, 'sns-integration/famtree.html',content)

class RelativeDetailView(DetailView,UserPassesTestMixin):
    model = RelativeProfile
    template_name = "sns-integration/relativeprofile_detail.html"

    def test_func(self):
        relative = self.get_object()
        if UserProfile.objects.get(user=self.request.user) == relative.relative:
            return True
        return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        twitter_id = context['object'].twitter_id
        instagram_id = context['object'].instagram_id
        if twitter_id:
            t = api.GetUser(user_id=twitter_id)
            context['twitter_handle'] = t.screen_name
        else:
            context['twitter_handle'] = ""
        if instagram_id:
            if os.path.exists(IG_CREDENTIAL_PATH):
                cl.load_settings(IG_CREDENTIAL_PATH)
                cl.login(IG_USERNAME, IG_PASSWORD)
                #print(cl.get_settings())
                print("login")
            else:
                cl.login(IG_USERNAME, IG_PASSWORD)
                cl.dump_settings(IG_CREDENTIAL_PATH)
                #print(cl.get_settings())
                print("login")
            context['instagram_handle'] = cl.username_from_user_id(instagram_id)
        else:
            context['instagram_handle'] = ""
        return context


class RelativeUpdateView(UpdateView,UserPassesTestMixin):

    model = RelativeProfile
    template_name = "sns-integration/relative-edit.html"
    form_class = RelativeForm
    
    def get_success_url(self):
           pk = self.kwargs["pk"]
           return reverse("relative-detail", kwargs={"pk": pk})

    def test_func(self):
        relative = self.get_object()
        if UserProfile.objects.get(user=self.request.user) == relative.relative:
            return True
        return False

class RelativeDeleteView(DeleteView,UserPassesTestMixin):

    model = RelativeProfile
    template_name = "sns-integration/relative-confirm-delete.html"
    success_url = "/famtree/"
    

    def test_func(self):
        relative = self.get_object()
        if UserProfile.objects.get(user=self.request.user) == relative.relative:
            return True
        return False

@login_required
def photobook(req):
    order_by = 'date'
    posts = []
    user = UserProfile.objects.get(user=req.user)
    relatives = user.relativeprofile_set.all()
    for relative in relatives:
        saved_posts = SavedPosts.objects.filter(relative=relative)
        for saved_post in saved_posts:
            if saved_post.sns_tag == "T":
                posts.append(getTweet(saved_post.post_id,relative,saved_post.saved_date))
            if saved_post.sns_tag == "F":
                posts.append(getFbPost(saved_post.post_id,relative,saved_post.saved_date))
            if saved_post.sns_tag == "I":
                if os.path.exists(IG_CREDENTIAL_PATH):
                    cl.load_settings(IG_CREDENTIAL_PATH)
                    cl.login(IG_USERNAME, IG_PASSWORD)
                    #print(cl.get_settings())
                    print("login")
                else:
                    cl.login(IG_USERNAME, IG_PASSWORD)
                    cl.dump_settings(IG_CREDENTIAL_PATH)
                    #print(cl.get_settings())
                    print("login")
                posts.append(get_instaPost(saved_post.post_id,relative,saved_post.saved_date,cl))
    posts = sorted(posts, key = lambda i: i[order_by],reverse=True)
    contents = {'posts':posts,
                'user': req.user}
    return render(req, 'sns-integration/photobook.html',contents)

@login_required
def help(req):
    return render(req, 'sns-integration/help.html')
@login_required
def help_dashboard(req):
    return render(req, 'help/help_dashboard.html')
@login_required
def help_famtree(req):
    return render(req, 'help/help_famtree.html')
@login_required
def help_addpeople(req):
    return render(req, 'help/help_addpeople.html')
@login_required
def help_album(req):
    return render(req, 'help/help_album.html')

@login_required
def settings(req):
    return render(req, 'sns-integration/settings.html')

@login_required
def settings_profile(req):
    user = req.user
    phone_number = None
    user_profile = UserProfile.objects.get(user=user)
    if user_profile.phone_number:
        x = phonenumbers.parse(str(user_profile.phone_number), None)
        phone_number = phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
    contents = {
        'user':user,
        'user_profile':user_profile,
        'phone_number':phone_number
    }
    return render(req, 'settings/settings_profile.html',contents)

@login_required
def settings_profile_edit(req):
    if req.method == "POST":
        detail1_form = UserDetailForm1(data = req.POST,instance = req.user)
        detail2_form = UserDetailForm2(data = req.POST, instance = UserProfile.objects.get(user=req.user))
        print(req.POST)
        if detail1_form.is_valid() and detail2_form.is_valid():
            detail1_form.save()
            detail2_form.save()
            print("Okay!")
            return redirect('settings_profile')
    else:
        print("get signal")
        detail1_form = UserDetailForm1(instance = req.user)
        detail2_form =  UserDetailForm2(instance = UserProfile.objects.get(user=req.user))
    forms = {
            'detail1_form': detail1_form,
            'detail2_form': detail2_form,
        }
    return render(req, 'settings/settings_profile_edit.html', forms)

@login_required
def settings_notification(req):
    
    return render(req, 'settings/settings_notification.html')
@login_required
def settings_privacy(req):
    return render(req, 'settings/settings_privacy.html')
@login_required
def settings_screen(req):
    return render(req, 'settings/settings_screen.html')
@login_required
def settings_language(req):
    return render(req, 'settings/settings_language.html')
@login_required
def settings_app_information(req):
    return render(req, 'settings/settings_app_information.html')

def register_intro(req):
    return render(req, 'sns-integration/register_intro.html')

def register_info1(req):
    slide_error = -1
    if req.method == "POST":
        form = UserRegForm(req.POST)
        if form.is_valid():
            form.save()
            print("Okay!")
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(req, user)
            return redirect('register_info2')
        else:
            if 'username' in form.errors:
                slide_error = 1
            else:
                slide_error= 2
    else:
        print("get signal")
        form = UserRegForm()
    return render(req, 'sns-integration/register_info1.html', {'form': form,'slide_error':slide_error})

@login_required
def register_info2(req):
    try:
        UserProfile.objects.get(user=req.user)
    except UserProfile.DoesNotExist:
        UserProfile.objects.create(user=req.user)
    slide_error = -1
    if req.method == "POST":
        detail1_form = UserDetailForm1(data = req.POST,instance = req.user)
        detail2_form = UserDetailForm2(data = req.POST, instance = UserProfile.objects.get(user=req.user))
        if detail1_form.is_valid() and detail2_form.is_valid():
            detail1_form.save()
            detail2_form.save()
            print("Okay!")
            return redirect('register_info3')
        else:
            if detail1_form.errors:
                slide_error=0
            elif 'birthday' in detail2_form.errors:
                slide_error = 1
            else :
                slide_error= 2
    else:
        print("get signal")
        detail1_form = UserDetailForm1()
        detail2_form =  UserDetailForm2()
    forms = {
            'detail1_form': detail1_form,
            'detail2_form': detail2_form,
            'slide_error':slide_error
        }
    return render(req, 'sns-integration/register_info2.html', forms)
@login_required
def register_info3(req):
    return render(req, 'sns-integration/register_info3.html')


@login_required
def relative_info(req):
    users = UserProfile.objects.get(user=req.user)
    if req.method == "POST":
        relative_form = RelativeForm(data = req.POST)
        print(req.POST)
        if relative_form.is_valid():
            instance = relative_form.save(commit=False)
            instance.relative = users
            instance.save()
            print(type(instance.pk))
            pk = instance.pk
            print("Okay!")
            return redirect('relative_sns',pk=pk)
    else:
        print("get signal")
        
        relative_form = RelativeForm()
    form = {
            'relative_form': relative_form,
        }
    return render(req, 'sns-integration/relative_info.html', form)

@my_user_passes_test(user_check, login_url = "/accounts/login/")
def relative_photo(req,pk):
    relative = RelativeProfile.objects.get(pk=pk)
    org_path = False
    rel_instagram_url = ''
    rel_twitter_url = ''
    base64_i = ''
    base64t = ''
    data = {'avatar': '',
         'avatar_url': '',
        }
    if relative.twitter_id:
        rel_twitter = api.GetUser(relative.twitter_id)
        rel_twitter_url = rel_twitter.profile_image_url
        rel_twitter_url = rel_twitter_url.replace("_normal", "")
        base64t = convertBase64(rel_twitter_url)
    if relative.instagram_id:
        if os.path.exists(IG_CREDENTIAL_PATH):
            cl.load_settings(IG_CREDENTIAL_PATH)
            cl.login(IG_USERNAME, IG_PASSWORD)
            #print(cl.get_settings())
            print("login")
        else:
            cl.login(IG_USERNAME, IG_PASSWORD)
            cl.dump_settings(IG_CREDENTIAL_PATH)
            #print(cl.get_settings())

            print("login")
        rel_instagram = cl.user_info(relative.instagram_id)
        rel_instagram = rel_instagram.dict()
        rel_instagram_url = rel_instagram['profile_pic_url']
        base64_i = convertBase64(rel_instagram_url)

    if 'edit' in req.path:
        data['avatar'] = relative.avatar
        data['avatar_url'] = relative.avatar_url

    if req.method == "POST":
        relative_photo_form = RelativePhoto(data = req.POST or None, instance=RelativeProfile.objects.get(pk=pk),files=req.FILES)
        if 'edit' in req.path:
            relative_photo_form.initial = data
        print(req.POST)
        if relative_photo_form.is_valid():
            instance = relative_photo_form.save(commit=False)
            #relative_photo_form.save()
            if instance.avatar_url and 'avatar_url' in relative_photo_form.changed_data:
                instance.avatar = ""
            instance.save()
            print("Okay!")
            if 'edit' in req.path:
                print("edit")
                return redirect('relative-detail',pk=pk)
            else:
                return redirect('home')
    else:
        print("get signal")
        if 'edit' in req.path:
            org_path = True
        relative_photo_form = RelativePhoto(instance=RelativeProfile.objects.get(pk=pk))
    form = {
            'relative_photo_form': relative_photo_form,
            'pk': pk,
            'relative':relative,
            'avatar_url': convertBase64(relative.avatar_url),
            'twitter_url64': base64t,
            'instagram_url64': base64_i,
            'twitter_url':rel_twitter_url,
            'instagram_url':rel_instagram_url,
            'path':org_path
        }
    return render(req, 'sns-integration/relative_photo.html', form)

@my_user_passes_test(user_check, login_url = "/accounts/login/")
def relative_sns(req,pk):
    org_path = False
    #users = UserProfile.objects.get(user=req.user)
    if FaceboookProfiles.objects.exists() == False:
        load_test_users()
    data = {'twitter_id': '',
         'instagram_id': '',
        'facebook_id': ''}

    instance=RelativeProfile.objects.get(pk=pk)
    if 'edit' in req.path:
        print("edit")
        org_path = True
        data['facebook_id'] = instance.facebook_id
        if instance.twitter_id:
            twitter_user = api.GetUser(user_id= instance.twitter_id)
            #instance.twitter_id = twitter_user.screen_name
            data['twitter_id'] = twitter_user.screen_name
        if instance.instagram_id:
            instagram_id = cl.username_from_user_id(instance.instagram_id)
            #instance.instagram_id = instagram_id
            data['instagram_id'] = instagram_id
        print(instance)
    if req.method == "POST":
        relative_sns_form = RelativeSNSForm(data = req.POST, instance=RelativeProfile.objects.get(pk=pk))
        if 'edit' in req.path:
            print("Data: ", data)
            relative_sns_form.initial = data
        print(req.POST)
        if relative_sns_form.is_valid():
            instance = relative_sns_form.save(commit=False)
            if instance.twitter_id:
                twitter_user = api.GetUser(screen_name = instance.twitter_id)
                instance.twitter_id = twitter_user.id
            if instance.instagram_id:
                instagram_id = cl.user_id_from_username(instance.instagram_id)
                instance.instagram_id = instagram_id
            instance.save()
            print(instance.twitter_id)
            print(instance.instagram_id)
            print("Okay!")
            if 'edit' in req.path:
                print("edit")
                return redirect('relative-detail',pk=pk)
            else:
                return redirect('relative_photo',pk=pk)

    else:
        print("get signal")
        relative_sns_form = RelativeSNSForm(initial=data)
    context = {
            'relative_sns_form': relative_sns_form,
            'pk': pk,
            'path': org_path
        }
    return render(req, 'sns-integration/relative_sns.html', context)

@login_required
def save_post(req,post_id,sns_tag,relative_pk):
    print("in")
    if req.method == 'POST':
            return HttpResponse("Request method is not a GET")
    if req.method =="GET":
        user = UserProfile.objects.get(user=req.user)
        relative = RelativeProfile.objects.get(pk=relative_pk)
        SavedPosts.objects.create(created_by=user,relative = relative,sns_tag = sns_tag[0].capitalize(),post_id=post_id,saved_date = datetime.now())
    return HttpResponse("Success.")
@login_required
def remove_saved_post(req,post_id,relative_pk):
    print("in")
    print(post_id)
    if req.method == 'POST':
            pass
    if req.method =="GET":
        user = UserProfile.objects.get(user=req.user)
        relative = RelativeProfile.objects.get(pk=relative_pk)
        SavedPosts.objects.filter(created_by=user,relative = relative,post_id=post_id).delete()
        messages.add_message(req, messages.SUCCESS, 'A post was removed.')
    return redirect('photobook')
@login_required
def remove_post(req,post_id,sns_tag,relative_pk):
    print("in")
    if req.method == 'POST':
            pass
    if req.method =="GET":
        user = UserProfile.objects.get(user=req.user)
        relative = RelativeProfile.objects.get(pk=relative_pk)
        DeletedPosts.objects.create(created_by=user,relative = relative,sns_tag = sns_tag[0].capitalize(),post_id=post_id,saved_date = datetime.now())
        messages.add_message(req, messages.SUCCESS, 'A post was removed.')
    return redirect('home')
@login_required
def filter(req):
    pb=False
    if req.method == "POST":
        filter = ','.join(map(str, req.POST.getlist("checkbox")))
        if filter and 'photobook' not in req.path:
            return redirect('filter_home',relatives=filter)
        elif filter and 'photobook' in req.path:
            return redirect('filter_photobook',relatives=filter)
        else:
            return redirect('home')
    else:
        users = UserProfile.objects.get(user=req.user)
        relatives = users.relativeprofile_set.all()
        if 'photobook' in req.path:
            tmp = SavedPosts.objects.filter(created_by=users).values('relative').distinct()
            tmp = [int(x['relative']) for x in tmp]
            relatives = RelativeProfile.objects.filter(pk__in=tmp)
    if 'photobook' in req.path:
        pb = True
    context = {
        'relatives':relatives,
        'pb':pb
    }   
    return render(req, 'sns-integration/filter.html', context) 
@login_required
def filter_home(req,relatives):
    relatives = [int(x) for x in relatives.split(',')]
    print(relatives)
    instances = RelativeProfile.objects.filter(pk__in=relatives)
    handles =[]
    r_names = []
    insta_handles=[]
    fb_handles=[]
    for info in instances:
        try:
            r_names.append(info.first_name + " " +  info.last_name + ", ")
        except:
            r_names.append(info.first_name + ", ")
        if info.twitter_id:
            print(info.twitter_id)
            handles.append(info.twitter_id) 
        if info.instagram_id:
            insta_handles.append(info.instagram_id)
        if info.facebook_id:
            fb_handles.append(info.facebook_id)
    r_names[-1] = r_names[-1].split(",")[0]
    contents = {"user": req.user,
            "profile": UserProfile.objects.get(user=req.user),
            'posts': getSnsPosts(handles, insta_handles,fb_handles,UserProfile.objects.get(user=req.user)),
            'filter':True,
            'rel_names':r_names
            }
    return render(req, 'sns-integration/home.html',contents)
@login_required
def filter_photobook(req,relatives):
    relatives = [int(x) for x in relatives.split(',')]
    #print(relatives)
    saved_posts = SavedPosts.objects.filter(relative__in=relatives)
    instances = RelativeProfile.objects.filter(pk__in=relatives)
    order_by = 'date'
    posts = []
    r_names = []
    for info in instances:
        try:
            r_names.append(info.first_name + " " +  info.last_name + ", ")
        except:
            r_names.append(info.first_name + ", ")
    r_names[-1] = r_names[-1].split(",")[0]
    for saved_post in saved_posts:
        if saved_post.sns_tag == "T":
            posts.append(getTweet(saved_post.post_id,saved_post.relative,saved_post.saved_date))
        if saved_post.sns_tag == "F":
            posts.append(getFbPost(saved_post.post_id,saved_post.relative,saved_post.saved_date))
        if saved_post.sns_tag == "I":
            if os.path.exists(IG_CREDENTIAL_PATH):
                cl.load_settings(IG_CREDENTIAL_PATH)
                cl.login(IG_USERNAME, IG_PASSWORD)
                #print(cl.get_settings())
                print("login")
            else:
                cl.login(IG_USERNAME, IG_PASSWORD)
                cl.dump_settings(IG_CREDENTIAL_PATH)
                #print(cl.get_settings())
                print("login")
            posts.append(get_instaPost(saved_post.post_id,saved_post.relative,saved_post.saved_date,cl))
    posts = sorted(posts, key = lambda i: i[order_by],reverse=True)
    contents = {'posts':posts,
                'user': req.user,
                'filter': True,
                'rel_names':r_names,}
    return render(req, 'sns-integration/photobook.html',contents)

def getSnsPosts(handles,insta_handles,fb_handles,user):
    start_time = time.time()
    sorted_posts = []
    tweets = []
    insta_posts = []
    fb_posts = []

    if handles:
        tweets = getTweets(handles,user)
    if insta_handles:
        insta_posts = get_instaPosts(insta_handles,user)
    if fb_handles:
        fb_posts = getFbPosts(fb_handles,user)

    sorted_posts = tweets + insta_posts + fb_posts
    sorted_posts = sorted(sorted_posts, key = lambda i: i['date'],reverse=True)
    print(len(sorted_posts))
    print(time.time()-start_time)
    return {'posts': sorted_posts}


def getTweets(handles,user):

    tweets = []
    #sort_tweets = []
    api = twitter.Api(consumer_key=API_Key,
                    consumer_secret=API_Key_Secret,
                    access_token_key=Access_Token,
                    access_token_secret=Access_Token_Secret, 
                    tweet_mode='extended')
    for handle in handles:
            relative = user.relativeprofile_set.get(twitter_id = handle)#RelativeProfile.objects.get(twitter_id = handle)
            saved_posts = SavedPosts.objects.filter(relative=relative,sns_tag = "T")
            removed_posts = DeletedPosts.objects.filter(relative=relative,sns_tag = "T")
            removed_id = set(removed_post.post_id for removed_post in removed_posts)
            try:
                names = relative.first_name + " " + relative.last_name
            except:
                names = relative.first_name
            video_url = ''
            avatar =''
            avatar_url =''
            if relative.avatar:
                avatar = relative.avatar.url
            elif relative.avatar_url:
                avatar_url = convertBase64( relative.avatar_url)
            relation = relative.get_relation_display
            person = api.GetUserTimeline(handle,count = 25)
            for tweet1 in person:
                    saved = False
                    if tweet1.retweeted_status != None:
                            status= tweet1.full_text.split(':')[0] + ": " +  tweet1.retweeted_status.full_text
                            status = status.split(' ')[:-1]
                    else:
                            status = tweet1.full_text
                            status = status.split(' ')
                    #print(status)
                    id = tweet1.id
                    if removed_posts:
                        if str(id) in str(removed_id):
                            continue
                    if saved_posts:
                        for saved_post in saved_posts:
                            if str(id) == str(saved_post.post_id):
                                saved = True
                    media =  tweet1.media
                    tweet_date = datetime.strptime(tweet1.created_at, '%a %b %d %H:%M:%S %z %Y')
                    if media:
                        for m in media:
                            if m.type == "video":
                                for info in m.video_info['variants']:
                                    if info['content_type'] == 'video/mp4' and info['bitrate'] > 900000 :
                                        video_url = info['url']
                    tweets.append({'id':id,'saved':saved,'status': status,'date': tweet_date, 'media': media,'video_url': video_url,
                    'names': names,'relative_pk':relative.pk,'avatar':avatar,'avatar_url':avatar_url,'relation':relation,'sns_tag': 'twitter'})    
    return tweets

def getTweet(post_id,relative,saved_date):
    try:
        names = relative.first_name + " " + relative.last_name
    except:
        names = relative.first_name
    relation = relative.get_relation_display
    video_url = ''
    avatar =''
    avatar_url =''
    if relative.avatar:
        avatar = relative.avatar.url
    elif relative.avatar_url:
        avatar_url = convertBase64( relative.avatar_url)
    tweet1 = api.GetStatus(post_id)
    if tweet1.retweeted_status != None:
            status= tweet1.full_text.split(':')[0] + ": " +  tweet1.retweeted_status.full_text
            status = status.split(' ')[:-1]
    else:
            status = tweet1.full_text
            status = status.split(' ')
    id = tweet1.id
    media =  tweet1.media
    tweet_date = datetime.strptime(tweet1.created_at, '%a %b %d %H:%M:%S %z %Y')
    if media:
        for m in media:
            if m.type == "video":
                for info in m.video_info['variants']:
                    if info['content_type'] == 'video/mp4' and info['bitrate'] > 900000 :
                        video_url = info['url']
    return({'id':id,'status': status,'date': tweet_date,'saved_date':saved_date, 'media': media,'video_url': video_url,
    'names': names,'avatar':avatar,'avatar_url':avatar_url,'relative_pk':relative.pk,'relation':relation,'sns_tag': 'twitter'})


def get_instaPosts(insta_handles,user):

    if os.path.exists(IG_CREDENTIAL_PATH):
        cl.load_settings(IG_CREDENTIAL_PATH)
        cl.login(IG_USERNAME, IG_PASSWORD)
        #print(cl.get_settings())
        print("login")
    else:
        cl.login(IG_USERNAME, IG_PASSWORD)
        cl.dump_settings(IG_CREDENTIAL_PATH)
        #print(cl.get_settings())

        print("login")
        
    insta_posts = []
    video_url = None
    img_url = None


    for handle in insta_handles:
        relative = user.relativeprofile_set.get(instagram_id = handle)
        relation = relative.get_relation_display
        saved_posts = SavedPosts.objects.filter(relative=relative,sns_tag = "I")
        removed_posts = DeletedPosts.objects.filter(relative=relative,sns_tag = "I")
        removed_id = set(removed_post.post_id for removed_post in removed_posts)
        try:
            names = relative.first_name + " " + relative.last_name
        except:
            names = relative.first_name
        avatar =''
        avatar_url =''
        if relative.avatar:
            avatar = relative.avatar.url
        elif relative.avatar_url:
            avatar_url = convertBase64( relative.avatar_url)
        posts = cl.user_medias(handle, amount= 20)
        for post in posts:
            post = post.dict()
            id = post['pk']
            if removed_posts:
                if str(id) in str(removed_id):
                    continue
            saved = False
            resources = []
            for saved_post in saved_posts:
                if str(id) == str(saved_post.post_id):
                    saved = True
            caption = post['caption_text']
            insta_date = post['taken_at'] #2021-11-12 21:36:34+00:00
            post_type = post['media_type']
            if post_type == 1:
                img_url = post['thumbnail_url']
                img_url = convertBase64(img_url)
            elif post_type == 2:
                video_url = post['video_url']
                video_url = convertBase64(video_url)
            else:
                for resource in post['resources']:
                    media_img_url = None
                    media_video_url = None
                    media_type = resource['media_type']
                    if media_type == 2 and resource['video_url']:
                        media_video_url = convertBase64(resource['video_url'])
                    if media_type == 1 and resource['thumbnail_url']:
                        media_img_url = convertBase64(resource['thumbnail_url'])
                    resources.append({'media_type':media_type,'media_video_url':media_video_url,'media_img_url':media_img_url})
            insta_posts.append({'id':id,'saved':saved,'caption': caption,'date': insta_date, 'post_type': post_type,'video_url': video_url,'img_url': img_url,
                            'resources': resources,'names': names,'avatar':avatar,'avatar_url':avatar_url,'relation':relation,'relative_pk':relative.pk,'sns_tag': 'instagram'})
            
    return insta_posts  


def get_instaPost(post_id,relative,saved_date,cl):
        
    video_url = None
    img_url = None
    relation = relative.get_relation_display
    try:
        names = relative.first_name + " " + relative.last_name
    except:
        names = relative.first_name
    avatar =''
    avatar_url =''
    if relative.avatar:
        avatar = relative.avatar.url
    elif relative.avatar_url:
        avatar_url = convertBase64( relative.avatar_url)
    post = cl.media_info(post_id)
    resources = []
    post = post.dict()
    id = post['pk']
    caption = post['caption_text']
    insta_date = post['taken_at'] #2021-11-12 21:36:34+00:00
    post_type = post['media_type']
    if post_type == 1:
        img_url = post['thumbnail_url']
        img_url = convertBase64(img_url)
    elif post_type == 2:
        video_url = post['video_url']
        video_url = convertBase64(video_url)
    else:
        for resource in post['resources']:
            media_img_url = None
            media_video_url = None
            media_type = resource['media_type']
            if media_type == 2 and resource['video_url']:
                media_video_url = convertBase64(resource['video_url'])
            if media_type == 1 and resource['thumbnail_url']:
                media_img_url = convertBase64(resource['thumbnail_url'])
            resources.append({'media_type':media_type,'media_video_url':media_video_url,'media_img_url':media_img_url})
    return({'id':id,'caption': caption,'date': insta_date,'saved_date':saved_date, 'post_type': post_type,'video_url': video_url,'img_url': img_url,
                            'resources': resources,'names': names,'avatar':avatar,'avatar_url':avatar_url,'relation':relation,'relative_pk':relative.pk,'sns_tag': 'instagram'})

def getFbPosts(fb_handles,user):

    media_p = []
    media_url = ''
    resource_url = ''
    fb_posts = []
    resources = []

    for handle in fb_handles:
        relative = user.relativeprofile_set.get(facebook_id = handle)
        relation = relative.get_relation_display
        saved_posts = SavedPosts.objects.filter(relative=relative,sns_tag = "F")
        removed_posts = DeletedPosts.objects.filter(relative=relative,sns_tag = "F")
        removed_id = set(removed_post.post_id for removed_post in removed_posts)
        fb_profile = FaceboookProfiles.objects.get(fb_id = handle)
        try:
            names = relative.first_name + " " + relative.last_name
        except:
            names = relative.first_name
        avatar =''
        avatar_url =''
        if relative.avatar:
            avatar = relative.avatar.url
        elif relative.avatar_url:
            avatar_url = convertBase64( relative.avatar_url)
        fb_api = GraphAPI(app_secret=APP_SECRET,app_id=APP_ID,access_token = str(fb_profile.fb_user_token))
        fb_feed = fb_api.get_full_connections(handle,connection="feed",count=15)
        fb_media = fb_api.get_full_connections(handle,connection="feed?fields=attachments",count=15)
        for m in fb_media['data']:
                media_id = m['id']
                if removed_posts:
                    if str(media_id) in str(removed_id):
                        continue
                if 'attachments' in m:
                    for media in m['attachments']['data']:
                        if media['type'] == 'photo':
                            media_url = media['media']['image']['src']
                            #print(media_url)
                            media_url = convertBase64(media_url)
                        elif media['type'] == 'video_inline':
                            media_url = media['media']['source']
                            #print(media_url)
                            media_url = convertBase64(media_url)
                        elif media['type'] == 'album':
                            for resource in media['subattachments']['data']:
                                if resource['type'] == 'photo':
                                    resource_url = resource['media']['image']['src']
                                    resource_url = convertBase64(resource_url)
                                elif resource['type'] == 'video_inline':
                                    resource_url = resource['media']['source']
                                    #print(resource_url)
                                    resource_url = convertBase64(resource_url)
                                resources.append({'type':resource['type'],'resource_url':resource_url})
                        media_p.append({'type':media['type'],'media_url':media_url,'media_id':media_id,'resources':resources})


        for data in fb_feed['data']:
            id = data['id']
            if removed_posts:
                if str(id) in str(removed_id):
                    continue
            saved = False
            for saved_post in saved_posts:
                if str(id) == str(saved_post.post_id):
                    saved = True
            media_post = []
            for media1 in media_p:
                if id == media1['media_id']:
                    media_post.append(media1)

            fb_date = datetime.strptime(data['created_time'], '%Y-%m-%dT%H:%M:%S%z') #'2021-09-27T10:10:26+0000 '
            if 'message' in data:
                status = data['message']
            else:
                status = None
            fb_posts.append({'id':id,'saved':saved,'status': status,'date': fb_date,'media':media_post,'sns_tag': 'facebook',
            'names': names,'relation':relation,'avatar':avatar,'avatar_url':avatar_url,'relative_pk':relative.pk})
    
    return fb_posts

def getFbPost(post_id,relative,saved_date):
    fb_profile = FaceboookProfiles.objects.get(fb_id = relative.facebook_id)
    fb_api = GraphAPI(app_secret=APP_SECRET,app_id=APP_ID,access_token =  str(fb_profile.fb_user_token))
    data = fb_api.get_object(post_id)

    try:
        names = relative.first_name + " " + relative.last_name
    except:
        names = relative.first_name
    relation = relative.get_relation_display
    avatar =''
    avatar_url =''
    if relative.avatar:
        avatar = relative.avatar.url
    elif relative.avatar_url:
        avatar_url = convertBase64( relative.avatar_url)
    media_p = []
    media_url = ''
    resource_url = ''
    resources = []
    fb_media = fb_api.get_full_connections(post_id,connection="attachments")
    for media in fb_media['data']:
        if media['type'] == 'photo':
            media_url = media['media']['image']['src']
            #print(media_url)
            media_url = convertBase64(media_url)
        elif media['type'] == 'video_inline':
            media_url = media['media']['source']
            #print(media_url)
            media_url = convertBase64(media_url)
        elif media['type'] == 'album':
            for resource in media['subattachments']['data']:
                if resource['type'] == 'photo':
                    resource_url = resource['media']['image']['src']
                    resource_url = convertBase64(resource_url)
                elif resource['type'] == 'video_inline':
                    resource_url = resource['media']['source']
                    #print(resource_url)
                    resource_url = convertBase64(resource_url)
                resources.append({'type':resource['type'],'resource_url':resource_url})
        media_p.append({'type':media['type'],'media_url':media_url,'resources':resources})


    id = data['id']
    fb_date = datetime.strptime(data['created_time'], '%Y-%m-%dT%H:%M:%S%z') #'2021-09-27T10:10:26+0000 '
    if 'message' in data:
        status = data['message']
    else:
        status = None
    return({'id':id,'saved_date':saved_date,'status': status,'date': fb_date,'media':media_p,'sns_tag': 'facebook',
    'names': names,'avatar':avatar,'avatar_url':avatar_url,'relation':relation,'relative_pk':relative.pk})






